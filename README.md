Mod to enhance factory automation in Starmade.

The mod introduces a new block, the "factory assistant". The assistant can be slaved to one factory or shipyard, and can have any number of factories slaved to it.

When it detects the master shipyard or factory is trying to pull a resource without finding any, the assistant tells the slave factories to start making the resource, if they are the correct type. If several resources are requested, the assistant will try to evenly distribute the resources among the factories.

Can be used recursively, where the slave factories have their own helpers, and their own slave factories. This should allow for the full automation of a production line.

The factory assistant does move any resources between inventories, the player is expected to set up their own lines using vanilla features.

The factory assistant is itself completely stateless, and so functions without a player interface.