package org.lupocani.starmade_mod.automanf;

import api.config.BlockConfig;
import api.listener.fastevents.BlockConfigLoadListener;
import api.listener.fastevents.FastListenerCommon;
import api.mod.StarMod;
import api.utils.StarRunnable;
import it.unimi.dsi.fastutil.shorts.ShortArrayList;
import org.apache.commons.lang3.ObjectUtils;
import org.lupocani.starmade_mod.automanf.computils.OtherModsListener;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;

import java.util.HashSet;
import java.util.Set;

public class Main extends StarMod {
    ElementInformation assistantBsc;
    ElementInformation assistantAdv;
    ManufactureListenerBasic listenerBsc;
    ManufactureListenerAdv listenerAdv;
    OtherModsListener listenerMods;
    /*
    public static final short[] pullers = new short[] {
            211, //basic
            217, //standard
            259, //advanced
            677, //shipyard
    };
    public static final short[] factories = new short[] {
            211, //basic
            217, //standard
            259, //advanced
    };
    public static final short[] inventories = new short[] {
            120, //storage
            211, //basic factory
            217, //standard factory
            259, //advanced factory
            677, //shipyard
    };//*/

    public static final short displayModule = 479;

    public static void main(String[] args) {
    }

    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        short[] textures = ElementKeyMap.getInfo(448).getTextureIds(); //textures from decorative computer (blue)
        textures[1] = textures[2]; textures[4] = textures[2]; textures[5] = textures[2];
        assistantBsc = BlockConfig.newElement(this, "Basic Factory Assistant", textures);
        assistantBsc.setBuildIconNum(ElementKeyMap.getInfo(ElementKeyMap.REPAIR_CONTROLLER_ID).buildIconNum);
        assistantBsc.setDescription("Is slaved to one factory or shipyard, and has other factories slaved to it." +
                "Makes the slave factories produce what the master factory/shipyard needs." +
                "Doesn't move any items.");


        assistantBsc.setMaxHitPointsE(100);
        assistantBsc.setArmorValue(1);
        assistantBsc.setCanActivate(false);
        BlockConfig.add(assistantBsc);

        //for (int id : pullers)      BlockConfig.setBlocksConnectable(ElementKeyMap.getInfo(id), assistantBsc);
        //for (int id : factories)    BlockConfig.setBlocksConnectable(assistantBsc, ElementKeyMap.getInfo(id));
        BlockConfig.addRecipe(assistantBsc, 3 /*basic factory*/, 5,
                new FactoryResource(10, (short) 440), //10 metal mesh
                new FactoryResource(10, (short) 220)); //10 crystal composite


        short[] texturesAdv0 = ElementKeyMap.getInfo(447).getTextureIds(); //textures from decorative computer (red)
        texturesAdv0[1] = texturesAdv0[2]; texturesAdv0[4] = texturesAdv0[2]; texturesAdv0[5] = texturesAdv0[2];

        assistantAdv = BlockConfig.newElement(this, "Advanced Factory Assistant", texturesAdv0);
        assistantAdv.setBuildIconNum(ElementKeyMap.getInfo(ElementKeyMap.MISSILE_DUMB_CONTROLLER_ID).buildIconNum);
        assistantAdv.setDescription("Is slaved to one factory or shipyard, and has other factories and inventories slaved to it. " +
                "Makes the slave factories produce what the master factory/shipyard needs. " +
                "Doesn't move any items.");


        assistantAdv.setMaxHitPointsE(100);
        assistantAdv.setArmorValue(1);
        assistantAdv.setCanActivate(false);
        BlockConfig.add(assistantAdv);

        //for (int id : pullers)      BlockConfig.setBlocksConnectable(ElementKeyMap.getInfo(id), assistantAdv);
        //for (int id : factories)    BlockConfig.setBlocksConnectable(assistantAdv, ElementKeyMap.getInfo(id));
        //for (int id : inventories)  BlockConfig.setBlocksConnectable(assistantAdv, ElementKeyMap.getInfo(id));
        BlockConfig.setBlocksConnectable(assistantAdv, ElementKeyMap.getInfo(displayModule));

        BlockConfig.addRecipe(assistantAdv, 5 /*advanced factory*/, 5,
                new FactoryResource(100, (short) 440), //100 metal mesh
                new FactoryResource(100, (short) 220)); //100 crystal composite

        listenerAdv.assistantID = assistantAdv.getId();
        listenerBsc.assistantID = assistantBsc.getId();
    }

    /** by lupoCani, 2021-10-??
     * Dynamically determines what blocks - possibly modded - should count as pullers (blocks which may request things
     * be produced), factories (blocks which may produce things) and inventories (blocks which may contain items).
     *
     * Is used to determine which blocks the AFA/BFA should be likable to.
     * Is triggered by a fast listener object held elsewhere.
     */
    public void onBlocksLoaded() {
        Set<ElementInformation> factories = new HashSet<ElementInformation>();
        Set<ElementInformation> inventories = new HashSet<ElementInformation>();
        Set<ElementInformation> pullers = new HashSet<ElementInformation>();

        for (ElementInformation element : ElementKeyMap.getInformationKeyMap().values()) {
            if (element == null) {
                Main.print("Warning - tried to process null element.");
                return;
            }
            if (element.getFactory() != null)
                factories.add(element);
            if (element.isInventory())
                inventories.add(element);
        }
        pullers.add(ElementKeyMap.getInfo(ElementKeyMap.SHIPYARD_COMPUTER));
        pullers.add(ElementKeyMap.getInfo(ElementKeyMap.STASH_ELEMENT));
        pullers.addAll(factories);
        inventories.removeAll(factories);


        for (ElementInformation element : factories) {
            try {
                BlockConfig.setBlocksConnectable(assistantBsc, element);
                BlockConfig.setBlocksConnectable(assistantAdv, element);
            } catch (Exception e) {
                Main.print("Failed to make factory "+element+" linkable to "+assistantAdv+" or "+assistantBsc+". ("+e+")");
            }
        }

        for (ElementInformation element : pullers) {
            try {
                BlockConfig.setBlocksConnectable(element, assistantBsc);
                BlockConfig.setBlocksConnectable(element, assistantAdv);
                Main.print(element + " now linkable to assistants.");
            } catch (Exception e) {
                Main.print("Failed to make puller "+element+" linkable to "+assistantAdv+" or "+assistantBsc+". ("+e+")");
            }
        }

        for (ElementInformation element : inventories) {
            try {
                BlockConfig.setBlocksConnectable(assistantAdv, element);
            } catch (Exception e) {
                Main.print("Failed to make inventory "+element+" linkable to "+assistantAdv+". ("+e+")");
            }
        }
    }

    @Override
    public void onEnable() {
        listenerBsc =  new ManufactureListenerBasic();
        listenerAdv =  new ManufactureListenerAdv();
        listenerMods = new OtherModsListener(this);

        FastListenerCommon.productionItemPullListeners.add(listenerBsc);
        FastListenerCommon.productionItemPullListeners.add(listenerAdv);
        FastListenerCommon.storageItemPullListeners.add(listenerBsc);
        FastListenerCommon.storageItemPullListeners.add(listenerAdv);
        FastListenerCommon.blockConfigLoadListeners.add(listenerMods);

        /*
        To reduce load, the processing of resource orders is spread over several ticks. The number of orders to process
        is determined dynamically such that all orders are processed within a second of being placed.
         */
        new StarRunnable(){ //Every tick, process some orders
            @Override
            public void run() {
                try {
                    listenerBsc.processOrders(10, 20);
                    listenerAdv.processOrders(1, 20);
                } catch (Exception e) {
                    Main.print("Exception occurred (processor).");
                    System.err.println(e.toString());
                    e.printStackTrace();
                }
            }
        }.runTimer(this,1);

    }

    public static void print(String string) {
        String marker = "(" + (false ? 's':'-') + '|' + (false ? 'c':'-') + ") ";
        System.err.println("[fae]"+ marker + string);
    }
}
