package org.lupocani.starmade_mod.automanf.cacheutils;


import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeProductInterface;
import org.schema.game.common.data.player.inventory.Inventory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/** by lupoCani, 2020
 * A class to efficiently query the quantity of elements available in a set of inventories. The quantity of a given
 * element is cached after it is first queried. The class then lets the user make hypothetical "withdrawals" of elements
 * from that cache, tracking persistently how much of a given element remains over the course of several withdrawals.
 * If the size of a withdrawal exceeds the remaining quantity, the quantity is set to 0.
 *
 * The main use case is shipyards, factories, etc.
 */
public class InvCache extends IdQuantMapCache {
    public final Set<Inventory> inventories = new HashSet<>();

    public void addInventories(Collection<Inventory> inventoryCollection) {
        inventories.addAll(inventoryCollection);
    }

    public void addInventory(Inventory inventory) {
        inventories.add(inventory);
    }

    @Override
    protected int fetch(short key){
        int out = 0;
        for (Inventory inventory : inventories) {
            out += inventory.getOverallQuantity(key);
        }
        return out;
    }

    /** by lupoCani, 2020
     * A method to track how many resources would remain if a given amount is removed.
     *
     * @param id The type of resource to withdraw
     * @param count The amount of the resource to be withdrawn
     * @return The remaining number of resources after the withdrawal (can be negative)
     */
    public int withdrawReturnBalance(short id, int count) {
        int result = incr(id, -count);

        if (result < 0)
            map.put(id, 0);

        return result;
    }

    /** by lupoCani, 2020
     * Determines how many of a given resource can be produced given the amount of resources in storage.
     * @param elementInformation The type of resource to produce
     * @return The amount of the resource which can be produced
     */
    public int getCanProduceCount(ElementInformation elementInformation) {
        int capability = Integer.MAX_VALUE;

        for (RecipeProductInterface recipeProductInterface : elementInformation.getProductionRecipe().getRecipeProduct())
            for (FactoryResource factoryResource : recipeProductInterface.getInputResource()) {
                capability = Math.min(capability, get(factoryResource.type)/factoryResource.count);
                if (capability < 1)
                    return 0;
            }

        return capability;
    }

    @Override
    public void clear() {
        super.clear();
        inventories.clear();
    }
}
