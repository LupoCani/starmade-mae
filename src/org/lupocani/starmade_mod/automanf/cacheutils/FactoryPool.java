package org.lupocani.starmade_mod.automanf.cacheutils;

import org.lupocani.starmade_mod.automanf.FactoryUtils;
import org.lupocani.starmade_mod.automanf.IdQuantPairSortable;
import org.schema.game.common.data.element.ElementInformation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class FactoryPool {
    private final IdQuantMapCache factories = new IdQuantMapCache();
    private final IdQuantMapCache producing = new IdQuantMapCache();
    private final Set<Short> producingSet = new HashSet<>();
    private final Set<Short> factoriesSet = new HashSet<>();

    public void addFactory(short id) {
        factories.incr(id, 1);
        factoriesSet.add(id);
    }

    public boolean couldProduce(ElementInformation element) {
        return factoriesSet.contains(FactoryUtils.getProdInType(element)); //drop-in replacement
    }

    public boolean canProduce(ElementInformation element) {
        if (producingSet.contains(element.id))
            return true;

        return factories.get(FactoryUtils.getProdInType(element)) > 0;
    }

    public void assignFactory(ElementInformation element, int count) {
        short type = element.id;
        factories.incr(FactoryUtils.getProdInType(element), -1);

        producingSet.add(type);
        producing.incr(type, count);
    }

    public boolean factoriesAvailable() {
        for (short factory : factoriesSet)
            if (factories.get(factory) > 0)
                return true;

        return false;
    }

    public Set<Short> getManifestIndex() {
        return producingSet;
    }

    public IdQuantMapCache getManifest() {
        return producing;
    }

    public void clear() {
        producingSet.clear();
        factoriesSet.clear();

        factories.clear();
        producing.clear();
    }
}
