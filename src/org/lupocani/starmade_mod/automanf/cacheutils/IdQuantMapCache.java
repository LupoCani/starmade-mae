package org.lupocani.starmade_mod.automanf.cacheutils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/** by lupoCani, 2020
 * A class containing a Short->Integer map where values are automatically initialized to some default value if queried
 * and found to be null. The .fetch() method, which specifies this default value, may be overridden to make the
 * default value conditional in some way on the value of the addressing key. Mainly useful for mapping element ids to
 * quantities without enumerating all elements.
 */
public class IdQuantMapCache {
    protected Map<Short,Integer> map = new HashMap<>();

    protected int fetch(short key) {
        return 0;
    }

    public int get(short key) {
        if (!map.containsKey(key))
            map.put(key, fetch(key));

        return map.get(key);
    }

    public int incr(short key, int value) {
        value += get(key);
        map.put(key, value);
        return value;
    }

    public Set<Short> getTouchedKeys() {
        return map.keySet();
    }

    public void clear(){
        map.clear();
    }
}
