package org.lupocani.starmade_mod.automanf;

import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.lupocani.starmade_mod.automanf.cacheutils.IdQuantMapCache;
import org.lupocani.starmade_mod.automanf.cacheutils.InvCache;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.ControlBlockElementCollectionManager;
import org.schema.game.common.controller.elements.FactoryAddOn;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.shipyard.orders.ShipyardEntityState;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.player.inventory.*;
import org.schema.game.common.util.FastCopyLongOpenHashSet;

import java.util.*;

/** by lupoCani, 2020
 * A class to list which resources a factory/shipyard needs, what assistants are available to make sure those resources
 * are produced, what factories are available to to those assistants to actually produce the resources in question,
 */
abstract public class ResourceOrder {
    private boolean isEmpty = true;

    //First stage resources
    public ManagedUsableSegmentController<?> musController; //The ManagedUsableSegmentController it's all happening on
    public final LongOpenHashSet factories = new LongOpenHashSet(); //Factories to order from
    public final LongOpenHashSet assistants = new LongOpenHashSet(); //A set of helpers who will try to carry out the order
    public long pullerIndex;
    protected final Stack<Integer> shortageBuffer = new Stack<>(); //Interim storage of the requested resources
    protected boolean isFactory = true;
    //Stores both item IDs and quantities as integers

    //Second stage resources
    protected final ArrayList<IdQuantPairSortable> shortageQuantities = new ArrayList<>(); //A list of elements requested, ordered by quantity
    protected final Set<FactoryCollectionManager> factories_ecm = new HashSet<>();

    /** by lupoCani, 2020
     * A method to identify the block of the pulling factory/shipyard, what assistants are slaved to the block, and what
     * factories are slaved to those assistants.
     * @param segmentPiece The control block of the factory/shipyard/storage.
     * @param assistantID The element id of the assistant type (basic/advanced) to look for.
     * @return Whether the factory/shipyard has any assistant, which has at least one slave factory.
     */
    private boolean basicSetup(SegmentPiece segmentPiece, short assistantID) {
        isEmpty = false;
        pullerIndex = segmentPiece.getAbsoluteIndex();


        SegmentController controller = segmentPiece.getSegmentController();
        if (controller instanceof ManagedUsableSegmentController)
            musController = (ManagedUsableSegmentController<?>) controller;
        else //The factory is on an asteroid or something. We'll add support for that ... later.
            return false;

        return findAssistants(assistantID) && findFactories();
    }

    //Identify assistants slaved to the pulling factory/shipyard
    public boolean findAssistants(short assistID) {
        Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> map = musController.getControlElementMap().getControllingMap().get(pullerIndex);

        if (map != null && map.get(assistID) != null) {
            assistants.addAll(map.get(assistID));
            return !assistants.isEmpty();
        }

        return false;
    }

    //Identify factories slaved to the assistant
    public boolean findFactories() {
        for (long typeIndex : assistants) {
            long posIndex = ElementCollection.getPosIndexFrom4(typeIndex);
            for (short type : ElementKeyMap.getFactorykeyset()) {
                Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> map =
                        musController.getControlElementMap().getControllingMap().get(posIndex);

                if (map != null && map.get(type) != null)
                    factories.addAll(map.get(type));
            }
        }
        return !factories.isEmpty();
    }

    /** by lupoCani on 2020-12-01 (based largely on Schema's pullResources method)
     * Determines what resources a shipyard needs.
     * @param cons The constructing state of the shipyard
     * @return Whether a valid order could be constructed
     */
    public boolean generalSetup(Constructing cons, short assistantID){
        ShipyardEntityState shipyardState = cons.getEntityState();
        if (!basicSetup(shipyardState.getShipyardCollectionManager().getControllerElement(), assistantID))
            return false;

        //isFactory = false; (removed while RRS factory speed is sorted out)
        InvCache invCache = new InvCache();
        invCache.addInventory(shipyardState.getInventory());

        for(short type : ElementKeyMap.keySet){
            int goal = shipyardState.currentMapFrom.get(type) - shipyardState.currentMapTo.get(type);
            if (goal <= 0) continue;

            short invType = FactoryUtils.manufacturedAsType(type);
            int deficit = -invCache.withdrawReturnBalance(invType, goal);

            if(deficit > 0) {
                shortageBuffer.push(deficit);
                shortageBuffer.push((int)invType);
            }
        }
        return true;
    }

    /** by lupoCani on 2020-12-01 (based largely on Schema's handleProduct method)
     * Determines what resources a factory needs.
     * @param factory The factory ECM.
     * @param assistantID The ElementInformation for the helper block.
     */
    public boolean generalSetup(FactoryCollectionManager factory, short assistantID) {
        if (!basicSetup(factory.getControllerElement(), assistantID))
            return false;

        Vector3i absolutePos = factory.getControllerElement().getAbsolutePos(new Vector3i());
        Inventory ownInventory = ((InventoryHolder) musController.getManagerContainer()).getInventory(ElementCollection.getIndex(absolutePos));
        final int prodLimit = ownInventory.getProductionLimit() > 0 ? ownInventory.getProductionLimit() : Integer.MAX_VALUE;

        int productCount = FactoryAddOn.getProductCount(factory.getCurrentRecipe());
        for (int productChainIndex = 0; productChainIndex < productCount; productChainIndex++) {
            for (FactoryResource s : FactoryAddOn.getInputType(factory.getCurrentRecipe(), productChainIndex)) {

                int ownCount = ownInventory.getOverallQuantity(s.type);
                int wantedCount = FactoryAddOn.getCount(s) * Math.min(prodLimit, factory.getFactoryCapability());
                if(ownCount < wantedCount){
                    shortageBuffer.push(wantedCount - ownCount);
                    shortageBuffer.push((int)s.type);
                }
            }
        }
        return true;
    }

    public boolean generalSetup(StashInventory inventory, SegmentPiece segmentPiece, short assistantID) {
        if (!basicSetup(segmentPiece, assistantID))
            return false;

        InventoryFilter filter = inventory.getFilter();
        InvCache invCache = new InvCache();
        invCache.addInventory(inventory);

        for (short type : filter.fillUpTo.getTypes()) {
            short invType = FactoryUtils.manufacturedAsType(type);
            int deficit = -invCache.withdrawReturnBalance(invType, filter.fillUpTo.get(invType));
            if (deficit <= 0) continue;

            shortageBuffer.push(deficit);
            shortageBuffer.push((int) invType);
        }

        return true;
    }

    /**
     * by lupoCani on 2020-12-02
     * Prepares the order for production by sorting the requested elements by quantity,
     * then converting them to the more convenient linked-list format for when they'll
     * change order a lot.
     * Also gets the factory ECMs from their indexes.
     */
    public void compile() {
        if (isClear()) return;
        //Properly list factories
        for (long factory : factories)
            factories_ecm.add(FactoryUtils.getFCMfromIndex(musController, factory));
        factories_ecm.remove(null);

        //Properly tabulate resources
        if (shortageBuffer.isEmpty())
            return;



        while (shortageBuffer.size() > 1) {
            int type = shortageBuffer.pop(); //We omit a bunch of validity checks here since the buffer is private.
            int quant = shortageBuffer.pop(); //We trust the values to be valid.
            shortageQuantities.add(new IdQuantPairSortable(quant, (short)type));
        }

        Collections.sort(shortageQuantities);
    }

    abstract public void process();

    /** by lupoCani on 2020-12-02
     * Prepares the order for recycling.
     */
    public void clear() {
        isEmpty = true;

        //First stage resources
        musController = null;
        factories.clear();
        assistants.clear();
        pullerIndex = -1;
        shortageBuffer.clear();
        isFactory = true;

        //Second stage resources
        shortageQuantities.clear();
        factories_ecm.clear();
    }

    public final boolean isClear() {
        return isEmpty;
    }
}
