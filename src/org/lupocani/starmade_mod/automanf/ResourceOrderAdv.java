package org.lupocani.starmade_mod.automanf;


import it.unimi.dsi.fastutil.shorts.Short2ObjectOpenHashMap;
import org.lupocani.starmade_mod.automanf.cacheutils.IdQuantMapCache;
import org.lupocani.starmade_mod.automanf.cacheutils.InvCache;
import org.schema.game.common.controller.SendableSegmentController;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeProductInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.util.FastCopyLongOpenHashSet;
import org.schema.game.server.controller.RequestDataPlanet;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class ResourceOrderAdv extends ResourceOrder {

    private final Set<Short> factories_type = new HashSet<Short>();
    private final IdQuantMapCache shoppingList = new IdQuantMapCache(); //Raw materials we're short on
    private final Set<Inventory> inventories = new HashSet<>();

    /** by lupoCani, 2020
     * A method to fully calculate which raw materials are needed to meet some manufacturing goal.
     *
     * A "raw material" is defined here as any resource which no connected factory type can produce, regardless of
     * whether there exists such a factory type in the game at the moment.
     *
     * To calculate the needed resources, the "crafting tree" is represented by a pair of stacks, and traversed
     * depth-first. An InvCache object is used to track which resources are available, and which have been reserved for
     * crafting something else earlier in the tree traversal.
     */
    private void expandTree() {
        IdQuantMapCache produceList = new IdQuantMapCache(); //Material to produce
        InvCache invCache = new InvCache(); //Cached reference to the inventories

        Stack<Short> stackID = new Stack<>();
        Stack<Integer> stackQuant = new Stack<>();

        invCache.addInventories(inventories);
        Collections.reverse(shortageQuantities);
        for (IdQuantPairSortable shortageQuantity : shortageQuantities) {
            stackID.push(shortageQuantity.type);
            stackQuant.push(shortageQuantity.count);
        }

        while (!stackID.isEmpty() && !stackQuant.isEmpty()) {

            //IdQuantPairSortable resource = stack.pop();
            short type = stackID.pop();
            int deficit = -invCache.withdrawReturnBalance(type, stackQuant.pop());
            if (deficit <= 0)
                continue; //If we have enough, great.

            ElementInformation elementInformation = ElementKeyMap.getInfoFast(type);
            if (!factories_type.contains(FactoryUtils.getProdInType(elementInformation))) //Do we have any factory that can make this stuff? (drop-in replacement)
            {
                shoppingList.incr(type, deficit); //It's a raw material, or we don't have the right factory.
                continue; //Nothing we can do, just ask the player to fetch some.
            }

            int manufCount = invCache.getCanProduceCount(elementInformation); //Can we produce some of the stuff immediately?

            if (manufCount > 0) { //If yes to both, assign a factory
                manufCount = Math.min(manufCount, deficit);
                deficit -= manufCount;

                produceList.incr(type, manufCount);

                //Directly the reserve the resources we'll be using to make it, since we know they exist.
                for (FactoryResource fr : FactoryUtils.getAllRecipeInputs(elementInformation.getProductionRecipe())){
                    invCache.withdrawReturnBalance(fr.type, fr.count * manufCount);
                }
            }

            //Add the rest of the resource bill to the stack, if any

            if (deficit > 0)
                for (RecipeProductInterface recipeProductInterface : elementInformation.getProductionRecipe().getRecipeProduct())
                    for (FactoryResource fr : recipeProductInterface.getInputResource()) {
                        stackID.push(fr.type);
                        stackQuant.push(fr.count * deficit);
                    }

        }

        shortageQuantities.clear();
        for (short type : produceList.getTouchedKeys()) {
            shortageQuantities.add(new IdQuantPairSortable(produceList.get(type), type));
        }
        Collections.sort(shortageQuantities);
        Collections.reverse(shortageQuantities);
    }

    private void findInventories() {
        for (long assistant_type : assistants) {
            long assistant_pos = ElementCollection.getPosIndexFrom4(assistant_type);
            for (short type : ElementKeyMap.inventoryTypes) {
                Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> map =
                        musController.getControlElementMap().getControllingMap().get(assistant_pos);

                if (map != null && map.get(type) != null)
                    for (long index : map.get(type)) {
                        Inventory inventory = ((InventoryHolder) musController.getManagerContainer()).getInventory(
                                ElementCollection.getPosIndexFrom4(index));

                        if (inventory != null)
                            inventories.add(inventory);
                    }
            }
        }
    }

    @Override
    public void compile() {
        super.compile();

        for (FactoryCollectionManager fcm : factories_ecm) {
            factories_type.add(fcm.getControllerElement().getInfo().id);
        }

        findInventories();
        expandTree();
    }

    @Override
    public void clear() {
        super.clear();

        factories_type.clear();
        shoppingList.clear();
        inventories.clear();
    }

    private void upddateDisplays() {
        StringBuilder displayString = new StringBuilder("Raw Resource Shortages: \n");
        if (shoppingList.getTouchedKeys().isEmpty())
            displayString.append("None.");
        for (short item : shoppingList.getTouchedKeys()) {
            ElementInformation element = ElementKeyMap.getInfoFast(item);
            int shortage = shoppingList.get(item);

            displayString.append(element.getName()).append(" (x ").append(shortage).append(") \n");
        }

        for (long assistant_type : assistants) {
            long assistant_pos = ElementCollection.getPosIndexFrom4(assistant_type);
            Short2ObjectOpenHashMap<FastCopyLongOpenHashSet> map =
                    musController.getControlElementMap().getControllingMap().get(assistant_pos);

            if (map != null && map.get(Main.displayModule) != null) {
                for (long dispModuleindex : map.get(Main.displayModule)) {

                    SegmentPiece sPiece = musController.getSegmentBuffer().getPointUnsave(ElementCollection.getPosIndexFrom4(dispModuleindex));
                    musController.getTextMap().put(sPiece.getTextBlockIndex(), displayString.toString());
                    try {
                        Method method = SendableSegmentController.class.getDeclaredMethod("sendTextBlockServerUpdate", SegmentPiece.class, String.class);
                        method.setAccessible(true);
                        method.invoke(musController, sPiece, displayString.toString());

                    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void process() {
        ArrayList<FactoryCollectionManager> factories_ll = new ArrayList<>(factories_ecm);
        Collections.sort(factories_ll, new FactoryUtils.FactoryCapacityComparatorReverse());

        for (IdQuantPairSortable resource: shortageQuantities)
            if (factories_ll.isEmpty())
                break;
            else
                for (ListIterator<FactoryCollectionManager> it = factories_ll.listIterator(); it.hasNext();){
                    FactoryCollectionManager fcm = it.next();

                    if (FactoryUtils.assignRecipe(fcm, resource.type)) {

                        FactoryUtils.setProdLimit(fcm, isFactory ?
                                resource.count :
                                (resource.count + 1) / 2); //factories run twice as fast as shipyards
                        FactoryUtils.setFactoryActive(fcm,true);
                        it.remove();

                        break;
                    }
                }

        for (FactoryCollectionManager fcm : factories_ll) {
            FactoryUtils.setFactoryActive(fcm,false);
        }

        upddateDisplays();
    }
}
