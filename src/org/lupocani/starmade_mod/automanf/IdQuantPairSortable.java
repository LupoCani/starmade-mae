package org.lupocani.starmade_mod.automanf;

import org.schema.game.common.data.element.FactoryResource;

public class IdQuantPairSortable implements Comparable<IdQuantPairSortable> {
    public short type;
    public int count;
    public IdQuantPairSortable(int quantity, short ID) {
        this.count = quantity;
        this.type = ID;
    }

    @Override
    public int compareTo(IdQuantPairSortable o) {
            return count - o.count;
    }
}
