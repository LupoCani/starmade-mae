package org.lupocani.starmade_mod.automanf;

public class ManufactureListenerAdv extends ManufactureListener<ResourceOrderAdv> {

    @Override
    protected ResourceOrderAdv getNewOrder() {
        return new ResourceOrderAdv();
    }
}
