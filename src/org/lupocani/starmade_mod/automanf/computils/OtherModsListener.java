package org.lupocani.starmade_mod.automanf.computils;

import api.config.BlockConfig;
import api.listener.fastevents.BlockConfigLoadListener;
import api.mod.StarMod;
import org.lupocani.starmade_mod.automanf.Main;
import org.schema.game.common.data.element.ElementCategory;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import java.util.HashSet;
import java.util.Set;

public class OtherModsListener implements BlockConfigLoadListener {
    Main main;

    public OtherModsListener(Main mod) {
        main = mod;
    }

    @Override
    public void onModLoadBlockConfig_PRE(StarMod starMod) {}

    @Override
    public void onModLoadBlockConfig_POST(StarMod starMod) {
        main.onBlocksLoaded();
    }

    @Override
    public void preBlockConfigLoad() {}

    @Override
    public void postBlockConfigLoad() {}
}
