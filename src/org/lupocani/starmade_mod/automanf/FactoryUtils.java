package org.lupocani.starmade_mod.automanf;

import api.config.BlockConfig;
import org.luaj.vm2.ast.Block;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.elements.*;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;
import org.schema.game.common.controller.elements.factory.FactoryUnit;
import org.schema.game.common.controller.elements.thrust.ThrusterCollectionManager;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementCollection;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.element.meta.RecipeProductInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.player.inventory.InventoryHolder;
import org.schema.game.common.data.player.inventory.NetworkInventoryInterface;
import org.schema.game.common.data.player.inventory.StashInventory;
import org.schema.schine.network.objects.remote.LongIntPair;
import org.schema.schine.network.objects.remote.RemoteLongIntPair;

import java.util.*;

import static org.schema.game.common.data.element.ElementInformation.*;

/** by lupoCani, 2020
 * A class to abstract away several complicated method invocations needed to query useful information about factories
 * and recipes.
 */
public class FactoryUtils {
    public static ArrayList<ElementCollectionManager<?,?,?>> getAllCollectionManagers(ManagedUsableSegmentController<?> ent) {
        ArrayList<ElementCollectionManager<?,?,?>> ecms = new ArrayList<ElementCollectionManager<?,?,?>>();

        for (ManagerModule<?, ?, ?> module : ent.getManagerContainer().getModules()) {
            if (module instanceof ManagerModuleCollection) {

                for (Object cm : ((ManagerModuleCollection<?, ?, ?>) module).getCollectionManagers()) {
                    ecms.add((ElementCollectionManager<?, ?, ?>) cm);
                }
            } else if (module instanceof ManagerModuleSingle) {
                ElementCollectionManager<?, ?, ?> cm = ((ManagerModuleSingle<?, ?, ?>) module).getCollectionManager();
                ecms.add(cm);
            }
        }
        return ecms;
    }

    public static short getSimlpleCollectionBlockID(ElementCollectionManager<?,?,?> elementCollectionManager) {
        if (elementCollectionManager.isDetailedElementCollections())
            return -2;

        if (elementCollectionManager instanceof ThrusterCollectionManager) {
            return 8; //Find thruster ID
        }

        return -1;
    }

    public static ControlBlockElementCollectionManager<?,?,?> getCBECMfromIndex(ManagedUsableSegmentController<?> ent, long index) {
        for (ManagerModule<?, ?, ?> module : ent.getManagerContainer().getModules()) {
            if (module instanceof ManagerModuleCollection) {
                for (ElementCollectionManager<?,?,?> cm : ((ManagerModuleCollection<?, ?, ?>) module).getCollectionManagers()) {
                    if (cm instanceof ControlBlockElementCollectionManager) {
                        if (((ControlBlockElementCollectionManager<?,?,?>) cm).getControllerIndex() == index) {
                            return ((ControlBlockElementCollectionManager<?,?,?>) cm);
                        }
                    }
                }
            }
        }
        return null;
    }

    public static FactoryCollectionManager getFCMfromIndex(ManagedUsableSegmentController<?> ent, long index) {
        ManagerContainer<?> managerContainer = ent.getManagerContainer();
        if (managerContainer instanceof FactoryAddOnInterface) {
            index = ElementCollection.getPosIndexFrom4(index);
            short ID = ent.getSegmentBuffer().getPointUnsave(index).getInfo().id;
            ManagerModuleCollection<FactoryUnit, FactoryCollectionManager, FactoryElementManager> mmc =
                    ((FactoryAddOnInterface) managerContainer).getFactory().map.get(ID);

            if (mmc != null){
                FactoryCollectionManager fcm = mmc.getElementManager().getCollectionManagersMap().get(index);
                if (fcm != null && fcm.getControllerElement().getAbsoluteIndex() == index) {
                    return fcm;
                }
            }
        }
        return null;
    }

    public static Set<Short> getAllRecipeOutputsSet(RecipeInterface recipeInterface) {
        Set<Short> out = new HashSet<Short>();
        if (recipeInterface != null)
            for (RecipeProductInterface recipeProduct : recipeInterface.getRecipeProduct()) {
                for (FactoryResource factoryResource : recipeProduct.getOutputResource()) {
                    out.add(factoryResource.type);
                }
            }
        return out;
    }

    public static Set<FactoryResource> getAllRecipeInputs(RecipeInterface recipeInterface) {
        Set<FactoryResource> out = new HashSet<>();
        for (RecipeProductInterface recipeProductInterface : recipeInterface.getRecipeProduct())
            out.addAll(Arrays.asList(recipeProductInterface.getInputResource()));

        return out;
    }

    public static void setFactoryActive(FactoryCollectionManager fcm, boolean active) {
        if (fcm == null) return;
        SegmentPiece factorySP = fcm.getControllerElement();
        factorySP.setActive(active);
        factorySP.applyToSegment(true);
        long index = ElementCollection.getEncodeActivation(factorySP, true, active, false);
        factorySP.getSegment().getSegmentController().sendBlockActivation(index);
    }

    /** by lupoCani, 2020
     * A method to tell a factory to produce a given element.
     *
     * @param fcm The collection manager of the factory
     * @param ID The id of the element to produce
     * @return Whether the assignment was successful
     */
    public static boolean assignRecipe(FactoryCollectionManager fcm, short ID) {
        ElementInformation resource = ElementKeyMap.getInfoFast(ID);
        if (getProdInType(resource) == fcm.getControllerElement().getInfo().getId()) {
            //We've found an element we can make.
            //Remove it from its current position in the queue and note that we're going to assign it.

            //For some reason, the authoritative field on what to produce
            // is the factory's inventory's "production" field, so set that
            // or it will overwrite the ProductionRecipe in the FCM.
            ManagedUsableSegmentController<?> controller = (ManagedUsableSegmentController<?>)fcm.getSegmentController();
            Inventory ownInventory = ((InventoryHolder) controller.getManagerContainer())
                    .getInventory(fcm.getControllerElement().getAbsoluteIndex());

            if (ownInventory instanceof StashInventory)
                ((StashInventory)ownInventory).setProduction(ID);
            else
                return false;

            fcm.setCurrentRecipe(resource.getProductionRecipe()); //Make the factory produce it.
            ownInventory.sendAll(); //Possibly fixes the network sync? (Nope, doesn't fix it.)

            //Send a network update
            long index4 = ElementCollection.getIndex4(
                    (short) ownInventory.getParameterX(),
                    (short) ownInventory.getParameterY(),
                    (short) ownInventory.getParameterZ(), ID);
            ((NetworkInventoryInterface) fcm.getSegmentController().getNetworkObject())
                    .getInventoryProductionBuffer().add(index4);

            return true;
        }
        else return false;
    }

    public static void setProdLimit(FactoryCollectionManager fcm, int limit) {
        Inventory inventory = ((InventoryHolder) ((ManagedUsableSegmentController<?>)fcm.getSegmentController()).getManagerContainer())
                .getInventory(fcm.getControllerElement().getAbsoluteIndex());
        if (inventory instanceof StashInventory)
            ((StashInventory) inventory).setProductionLimit(limit);

        ((ManagedSegmentController<?>) fcm.getSegmentController()).getManagerContainer().getInventoryNetworkObject()
                .getInventoryProductionLimitBuffer()
                .add(new RemoteLongIntPair(new LongIntPair(inventory.getParameter(), limit), false));
    }

    public static RecipeInterface getRealRecipe(FactoryCollectionManager fcm) {
        Inventory ownInventory = ((InventoryHolder) ((ManagedUsableSegmentController<?>) fcm.getSegmentController()).getManagerContainer())
                .getInventory(fcm.getControllerElement().getAbsoluteIndex());
        if (ElementKeyMap.isValidType(ownInventory.getProduction()))
            return ElementKeyMap.getInfoFast(ownInventory.getProduction()).getProductionRecipe();
        else
            return fcm.getCurrentRecipe();
    }

    /**
     * Checks the element id of the factory which produces a given element.
     * Is a work-around for a Starloader bug, until that's fixed.
     * @param element the element to check.
     * @return the element id of the factory which produces the element.
     */
    public static short getProdInType(ElementInformation element) {
        switch(element.getProducedInFactory()){
            case(FAC_CAPSULE): return ElementKeyMap.FACTORY_CAPSULE_ASSEMBLER_ID;
            case(FAC_MICRO): return ElementKeyMap.FACTORY_MICRO_ASSEMBLER_ID;
            case(FAC_BASIC): return ElementKeyMap.FACTORY_BASIC_ID;
            case(FAC_STANDARD): return ElementKeyMap.FACTORY_STANDARD_ID;
            case(FAC_ADVANCED): return ElementKeyMap.FACTORY_ADVANCED_ID;

            default:
                for (short id : BlockConfig.customFactories.keySet()) {
                    if (element.getProducedInFactory() == BlockConfig.customFactories.get(id))
                        return id;
                }
                return 0;
        }
    }

    public static class FactoryCapacityComparator implements Comparator<FactoryCollectionManager> {
        @Override
        public int compare(FactoryCollectionManager o1, FactoryCollectionManager o2) {
            return Integer.compare(o1.getFactoryCapability(), o2.getFactoryCapability());
        }
    }
    public static class FactoryCapacityComparatorReverse implements Comparator<FactoryCollectionManager> {
        @Override
        public int compare(FactoryCollectionManager o1, FactoryCollectionManager o2) {
            return Integer.compare(o2.getFactoryCapability(), o1.getFactoryCapability());
        }
    }

    public static short manufacturedAsType(short type) {
        if (!ElementKeyMap.isValidType(type))
            throw new IllegalArgumentException("Invalid element type " + type);

        int trueType = ElementKeyMap.getInfoFast(type).getSourceReference();

        return trueType == 0 ? type : (short) trueType;
    }
}
