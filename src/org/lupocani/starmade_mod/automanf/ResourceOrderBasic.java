package org.lupocani.starmade_mod.automanf;

import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.meta.RecipeInterface;

import java.util.*;

public class ResourceOrderBasic extends ResourceOrder {

    //Second stage resources
    public final Set<Short> shortageIDs = new HashSet<>(); //A set of which elements are requested
    public final List<Short> productionQueue = new LinkedList<Short>(); //A list of elements requested, sorted by priority.
    // Priority starts out matching quantity, then changes as factories are assigned.

    @Override
    public void compile() {
        super.compile();
        for (IdQuantPairSortable idQuantPairSortable : shortageQuantities) {
            shortageIDs.add(idQuantPairSortable.type);
            productionQueue.add(idQuantPairSortable.type);
        }
    }

    @Override
    public void clear() {
        super.clear();

        //Second stage resources
        factories_ecm.clear();
        shortageIDs.clear();
        productionQueue.clear();
    }

    private void bumpQueueFromSet(Set<Short> currently_produced) {
        for (ListIterator<Short> it = productionQueue.listIterator(); it.hasNext();) {
            if (currently_produced.contains(it.next())) {
                it.remove();
                //if (currently_produced.isEmpty()) break;
            }
        }
        productionQueue.addAll(currently_produced);
        currently_produced.clear();
    }

    public void process() {
        if (isClear()) return;

        //If no resources are needed, shut down the factories.
        if (shortageIDs.isEmpty()){
            for (FactoryCollectionManager factory : factories_ecm)
                FactoryUtils.setFactoryActive(factory, false);

            return;
        }

        //Assign production jobs to factories
        for (FactoryCollectionManager factory : factories_ecm) {

            //See if a job can be assigned to the factory that matches the order
            boolean hasJob = assignFactoryJob(factory);

            //If we weren't able to find a job for the factory. Shut it down until we can.
            FactoryUtils.setFactoryActive(factory, hasJob);
        }
    }

    /** By lupoCani on 2020-12-01
     * Given a factory, and an order for resources, tries to make the factory produce the least
     * needed resource.
     * @param factory The factory ECM
     * @return Whether the factory has a job
     */
    private boolean assignFactoryJob(FactoryCollectionManager factory) {
        ElementInformation factoryElement = factory.getControllerElement().getInfo();

        //Get what exactly is being made in the factory.
        //Slim it down to what overlaps with what we want.
        RecipeInterface recipe = FactoryUtils.getRealRecipe(factory);
        Set<Short> currently_produced = FactoryUtils.getAllRecipeOutputsSet(recipe);
        currently_produced.retainAll(shortageIDs);

        //Check every resource we want.
        short assignedID = 0;
        for (ListIterator<Short> it = productionQueue.listIterator();it.hasNext();) {
            short factoryResourceId = it.next();
            //Check that the factory is compatible.
            if (FactoryUtils.assignRecipe(factory, factoryResourceId)) {
                //We've found an element we can make.
                //Remove it from its current position in the queue and note that we're going to assign it.
                assignedID = factoryResourceId;
                it.remove();
                break;
            }
        }

        if (assignedID > 0) {
            productionQueue.add(assignedID); //Bump the element to the back of the queue.

            return true; //The factory has a job, return true
        } else {
            return false; //The factory does not have a job, return false
        }
    }
}
