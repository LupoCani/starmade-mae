package org.lupocani.starmade_mod.automanf;

import api.listener.fastevents.ProductionItemPullListener;
import api.listener.fastevents.StorageItemPullListener;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import org.schema.game.common.controller.elements.ManagerContainer;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.shipyard.orders.states.Constructing;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.player.inventory.StashInventory;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/** by lupoCani, 2020
 * A class to register factories/shipyards pulling items and placing so-called "resource orders" listing what resources
 * they lack for continued production. These orders are then processed to tell other factories to make more of the
 * lacking resources. The processing is entirely internal to the order object, this class handles only gathering
 * basic parameters for order creation, a queue of placed-but-unprocessed orders that is processed at a dynamic rate
 * to minimize lag, and finally a pool of "empty" orders (into which fully processed orders are recycled) to minimize
 * object creation during order placement.
 *
 * The class is parameterized to handle either basic or advanced resource orders respectively.
 *
 * @param <RO> The type of resource order (basic/advanced) the listener handles.
 */
abstract class ManufactureListener<RO extends ResourceOrder> implements ProductionItemPullListener, StorageItemPullListener {
    public short assistantID = -1;
    private final Queue<RO> resourceOrders = new LinkedList<>();
    private final Stack<RO> freshROs = new Stack<>();
    private int ordersProcessed = 0;

    /** by lupoCani on 2020-12-07
     * Provides a new, fresh order from the recycling bin,
     * to avoid object generation during the event call itself.
     * @return the new resource order.
     */
    private RO getFreshOrder() {
        if (!freshROs.isEmpty())
            if (freshROs.peek().isClear())
                return freshROs.pop();
            else
                freshROs.clear();

        return getNewOrder();
    }

    abstract protected RO getNewOrder();

    /** by lupoCani on 2020-12-07
     * Places an order back in the recycling bin.
     * @param resourceOrder the order to recycle.
     */
    private void returnOrder(RO resourceOrder) {
        if (Math.random() > .99) return; //Drop some orders, makes the pool shrink if too large
        resourceOrder.clear();
        freshROs.push(resourceOrder);
    }

    /** by lupoCani on 2020-12-07
     * Processes an amount of orders. This spread the actual process-heavy part over several ticks.
     * @param minimum The minimum amount of orders to process.
     * @param fraction The (inverse of the) fraction of all orders to process.
     */
    final public void processOrders(int minimum, int fraction) {
        int processCap = Math.max(ordersProcessed, resourceOrders.size()/fraction + minimum);
        ordersProcessed = 0;

        while (!resourceOrders.isEmpty() && ordersProcessed < processCap) {
            RO resourceOrder = resourceOrders.remove();
            resourceOrder.compile();
            resourceOrder.process();
            returnOrder(resourceOrder);
            ordersProcessed++;
        }
    }

    @Override
    public void onPrePull(FactoryCollectionManager var1, Constructing var2) {};

    @Override
    public void onPostPull(FactoryCollectionManager factoryCollectionManager, Constructing constructing) {
        try {
            RO resourceOrder = getFreshOrder();
            boolean placeOrder = false;

            //Tabulate the resources needed and basic facts about the request.
            if (factoryCollectionManager != null) {
                placeOrder = resourceOrder.generalSetup(factoryCollectionManager, assistantID);
            }
            else if (constructing != null) {
                placeOrder = resourceOrder.generalSetup(constructing, assistantID);
            }
            else return;

            if (placeOrder)
                resourceOrders.add(resourceOrder);
            else
                returnOrder(resourceOrder);

        } catch (Exception e) {
            Main.print("Exception occurred (factory/shipyard listener).");
            System.err.println(e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public void onPreItemPull(@NotNull StashInventory puller, @NotNull SegmentPiece segmentPiece, boolean transferOk){}
    @Override
    public void onItemPullChecks(ManagerContainer managerContainer, LongOpenHashSet longOpenHashSet) {}

    @Override
    public void onPostItemPull(@NotNull StashInventory puller, @NotNull SegmentPiece segmentPiece, boolean transferOk){
        try {
            RO resourceOrder = getFreshOrder();
            boolean placeOrder = resourceOrder.generalSetup(puller, segmentPiece, assistantID);

            if (placeOrder)
                resourceOrders.add(resourceOrder);
            else
                returnOrder(resourceOrder);

        } catch (Exception e) {
            Main.print("Exception occurred (storage listener).");
            System.err.println(e.toString());
            e.printStackTrace();
        }
    };
}
